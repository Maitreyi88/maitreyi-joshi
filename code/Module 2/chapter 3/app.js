function find() {
    var a1 = document.getElementById("t1");
    var b1 = document.getElementById("t2");
    var a2 = document.getElementById("t3");
    var b2 = document.getElementById("t4");
    var a3 = document.getElementById("t5");
    var b3 = document.getElementById("t6");
    var a4 = document.getElementById("t7");
    var b4 = document.getElementById("t8");
    //co-ordinates
    var x_1 = +(a1.value);
    var y_1 = +(b1.value);
    var x_2 = +(a2.value);
    var y_2 = +(b2.value);
    var x_3 = +(a3.value);
    var y_3 = +(b3.value);
    var x_4 = +(a4.value);
    var y_4 = +(b4.value);
    var calarea = (x_1 * (y_2 - y_3) + x_2 * (y_3 - y_1) + x_3 * (y_1 - y_2)) * 0.5;
    if (calarea < 0) {
        calarea = calarea * (-1);
    }
    var calarea1 = (x_1 * (y_2 - y_4) + x_2 * (y_4 - y_1) + x_4 * (y_1 - y_2)) * 0.5;
    if (calarea1 < 0) {
        calarea1 = calarea * (-1);
    }
    var calarea2 = (x_1 * (y_4 - y_3) + x_4 * (y_3 - y_1) + x_3 * (y_1 - y_4)) * 0.5;
    if (calarea2 < 0) {
        calarea2 = calarea2 * (-1);
    }
    //area of point considered
    var calarea3 = (x_4 * (y_2 - y_3) + x_2 * (y_3 - y_4) + x_3 * (y_4 - y_2)) * 0.5;
    if (calarea3 < 0) {
        calarea3 = calarea3 * (-1);
    }
    var sum = calarea1 + calarea2 + calarea3;
    // alert(sum);
    if (calarea == 0.0000000000 && sum == 0.0000000000) {
        document.getElementById("p1").innerHTML = "Input is NULL.";
    }
    else {
        if ((calarea - sum) < 0.0000000001) {
            document.getElementById("p1").innerHTML = "The point lies inside the triangle";
        }
        else {
            document.getElementById("p1").innerHTML = "The point lies outside the triangle";
        }
    }
}
//# sourceMappingURL=app.js.map