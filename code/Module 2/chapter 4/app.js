function table() {
    var n1 = (document.getElementById("t1"));
    var table = document.getElementById("tb1");
    var n = +n1.value;
    while (table.rows.length > 0) {
        table.deleteRow(0);
    }
    for (var i = 1; i <= n; i++) {
        var row = table.insertRow();
        var column = row.insertCell();
        var t = document.createElement("input");
        t.type = "text";
        t.style.borderColor = "brown";
        t.style.backgroundColor = "#E6DADA";
        var b = (n * i).toString();
        t.value = n.toString() + " * " + i + "  =  " + b;
        column.appendChild(t);
    }
}
//# sourceMappingURL=app.js.map